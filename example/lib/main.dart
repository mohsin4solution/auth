import 'package:auth/auth.dart';
import 'package:auth/auth/routes/AuthAppPagesConstants.dart';
import 'package:auth/auth/routes/authrouter.dart';
import 'package:auth/auth/screens/login/LoginScreen.dart';
import 'package:example/route/globals.dart';
import 'package:flutter/material.dart';
import 'package:network/SharedPrefManager.dart';
import 'package:network/models/SettingsBms.dart';
import 'package:network/models/theme/ThemeProvider.dart';
import 'package:provider/provider.dart';


Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  Auth.initAuth("http://192.168.18.14:8090/");

  SettingsBms settingsBms = SettingsBms();
  SharedPrefManagerUser? pref = await SharedPrefManagerUser.getInstance();
  if(pref!=null){
    settingsBms = pref.getSettings();
  }

  runApp(ChangeNotifierProvider(
    create: (_) => ThemeProvider(isLightTheme: true),
    child: AppStart(),
  ));

}


class AppStart extends StatelessWidget {
  const AppStart({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    ThemeProvider themeProvider = Provider.of<ThemeProvider>(context);
    return MyApp(
      themeProvider: themeProvider,
    );
  }
}



class MyApp extends StatefulWidget with WidgetsBindingObserver {
  final ThemeProvider themeProvider;

  const MyApp({Key? key, required this.themeProvider}) : super(key: key);
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: widget.themeProvider.themeData(),

      debugShowCheckedModeBanner: false,
      navigatorObservers: [
        AppGlobals.routeObserver,
      ],
      onGenerateRoute: AuthRouter.generateRoute,
      initialRoute: AuthAppPagesConstants.loginPageRoute,
      home: LoginScreen(),

      title: "Login",

      // localizationsDelegates: [
      //   AppLocalizationDelegate(),
      //   GlobalMaterialLocalizations.delegate,
      //   GlobalWidgetsLocalizations.delegate,
      //   GlobalCupertinoLocalizations.delegate,
      // ],
      supportedLocales: [
        const Locale('en', ''), // English, no country code
        const Locale('ar', ''), // Arabic, no country code
      ],

    );
  }
}

