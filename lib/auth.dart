library auth;

import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';

import 'auth/configuration/Constants.dart';
import 'auth/repo/AuthRepo.dart';

/// A Calculator.
class Auth {


  static void initAuth(String pBaseUrl) {
    Constants.baseUrl = pBaseUrl;
   if( !GetIt.I.isRegistered<AuthRepo>())
    {
      GetIt.I.registerLazySingleton(() {
        return AuthRepo();
      });
    }
  }

  static void setUrl(String pBaseUrl) {
    Constants.baseUrl = pBaseUrl;
  }

}