import 'package:flutter/material.dart';

class AuthPageNotFound extends StatelessWidget {
  final String? name;
  AuthPageNotFound(this.name);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Page not found"),
      ),
      body: Container(
        color: Colors.grey[100],
        child: Center(
          child: Text(
            "Page not found $name",
            style: TextStyle(
              fontSize: 20.0,
              color: Colors.black,
            ),
          ),
        ),
      ),
    );
  }
}
