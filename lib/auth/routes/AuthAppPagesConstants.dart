class AuthAppPagesConstants {
  static const String loginPageRoute = "/loginPage";
  static const String registerPageRoute = "/registerPage";
  static const String tempLangPageRoute = "/tempLangPageRoute";
  static const String mainScreenBMSProPageRoute = "/mainScreenPageMBSPro";
}
