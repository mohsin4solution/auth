import 'package:auth/auth/configuration/Constants.dart';
import 'package:auth/auth/routes/AuthAppPagesConstants.dart';
import 'package:auth/auth/routes/AuthPageNotFound.dart';
import 'package:auth/auth/screens/lang/AllLanguages.dart';
import 'package:auth/auth/screens/login/LoginScreen.dart';
import 'package:auth/auth/screens/register/registerScreen.dart';
import 'package:flutter/material.dart';

class AuthRouter {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case AuthAppPagesConstants.loginPageRoute:
        return _NoAnimationMaterialPageRoute<dynamic>(
          builder: (_) => LoginScreen(),
          settings: settings,
        );
        break;
      case AuthAppPagesConstants.registerPageRoute:
        return _NoAnimationMaterialPageRoute<dynamic>(
          builder: (_) => RegisterScreen(),
          settings: settings,
        );
        break;
      case AuthAppPagesConstants.tempLangPageRoute:
        return _NoAnimationMaterialPageRoute<dynamic>(
          builder: (_) => AllLanguages(),
          settings: settings,
        );
        break;
      default:
        {
          RouteSettings routeSettings = RouteSettings(name: Constants.PAGE_NOT_FOUND);
          return MaterialPageRoute(
              builder: (context) => AuthPageNotFound(settings.name),
              settings: routeSettings);
        }
    }
  }
}

/// A MaterialPageRoute without any transition animations.
class _NoAnimationMaterialPageRoute<T> extends MaterialPageRoute<T> {
  _NoAnimationMaterialPageRoute({
    required WidgetBuilder builder,
    bool maintainState = true,
    bool fullscreenDialog = false,
    RouteSettings? settings,
  }) : super(
          builder: builder,
          maintainState: maintainState,
          settings: settings,
          fullscreenDialog: fullscreenDialog,
        );

  @override
  Widget buildPage(
    BuildContext context,
    Animation<double> animation,
    Animation<double> secondaryAnimation,
  ) {
    return builder(context);
  }

  @override
  Widget buildTransitions(
    BuildContext context,
    Animation<double> animation,
    Animation<double> secondaryAnimation,
    Widget child,
  ) {
    return new FadeTransition(opacity: animation, child: child);
    //  return new RotationTransition(
    //      turns: animation,
    //      child: new ScaleTransition(
    //        scale: animation,
    //        child: new FadeTransition(
    //          opacity: animation,
    //          child: child,
    //        ),
    //      ));

    // return child;    // not animation
  }
}
