
import 'package:flutter/material.dart';

import 'CustomRaisedButton.dart';

class RaisedButtonLeftImage extends CustomRaisedButton {
  RaisedButtonLeftImage({
    required String assetName,
    required String text,
    Color? color,
    Color? textColor,
    VoidCallback? onPress,
  }) : super(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Image.asset(assetName),
              Text(
                text,
                style: TextStyle(
                  color: textColor,
                  fontSize: 15.0,
                ),
              ),
              Opacity(
                opacity: 0.0,
                child: Image.asset(assetName),
              ),
            ],
          ),
          color: color,
          onPress: onPress,
        );
}
