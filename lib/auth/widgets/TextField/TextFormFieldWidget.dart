import 'package:flutter/material.dart';


// initalVal or controller should be null at a time. Both can't be set. One should be null.
class TextFormFieldWidget extends StatefulWidget {
  String initalVal;
  bool isPassword;
  String hint;
  String lable;
  Function onSave;
  Function? onChange;
  Function? validate;
  TextInputType textInputType;
  TextCapitalization textCapitalization;
  int minLine;

  int maxLines;
  int? maxLength;
  bool isExpend;

  bool isEnable;

  TextEditingController? controller;

  TextFormFieldWidget({
    this.controller,
    this.initalVal = "",
    this.isPassword = false,
    this.isEnable = true,
    this.hint = "",
    this.lable = "",
    required this.onSave,
    this.validate,
    this.onChange,
    this.textInputType = TextInputType.text,
    this.textCapitalization = TextCapitalization.none,
    this.minLine = 1,
    this.maxLines = 1,
    this.maxLength,
    this.isExpend = false,
  }); //:controller = controller ?? new TextEditingController();

  @override
  _TextFormFieldWidgetState createState() => _TextFormFieldWidgetState();
}

class _TextFormFieldWidgetState extends State<TextFormFieldWidget> {
  @override
  Widget build(BuildContext context) {
    return TextFormField(
      enabled: widget.isEnable,
      decoration: InputDecoration(
        counterText: "",
        hintText: widget.hint,
        labelText: widget.lable,
        focusColor: Theme.of(context).primaryColor,
        suffixIcon: widget.isPassword
            ? TextButton.icon(
                label: SizedBox.shrink(),
                icon:
                    Icon(Icons.remove_red_eye),
                onPressed: () {
                  setState(() {
                    widget.isPassword = widget.isPassword ? false : true;
                  });
                },
              )
            : null,
        border: OutlineInputBorder(
          borderRadius: BorderRadius.all(Radius.circular(10.0)),
        ),
      ),
      expands: widget.isExpend,
      textCapitalization: widget.textCapitalization,
      keyboardType: widget.textInputType,
      obscureText: widget.isPassword,
      initialValue: widget.initalVal,
      validator: widget.validate as String? Function(String?)?,
      onChanged: widget.onChange as void Function(String)?,
      maxLines: widget.maxLines,
      minLines: widget.minLine,
      maxLength: widget.maxLength,
      onSaved: widget.onSave as void Function(String?)?,
      controller: widget.controller ?? null,
    );
  }
}
