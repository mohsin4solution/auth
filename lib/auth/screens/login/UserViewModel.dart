import 'package:auth/auth/repo/AuthRepo.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:network/HttpService.dart';
import 'package:network/models/Token.dart';

class UserViewModel extends ChangeNotifier {

  UserViewModel();

  bool _isError = false;
  bool _isData  = true;
  bool _isCallingService = false;
  String? _errorMessage;

  AuthRepo? get repository => GetIt.I<AuthRepo>();

  bool get isError => _isError;

  set isError(bool value) {
    _isError = value;
  }

  bool get isData => _isData;

  set isData(bool value) {
    _isData = value;
  }

  bool get isCallingService => _isCallingService;

  set isCallingService(bool value) {
    _isCallingService = value;
    notifyListeners();
  }

  String? get errorMessage => _errorMessage;

  set errorMessage(String? value) {
    _errorMessage = value;
  }

  Future<APIRespons> login(String? userName, String? password) async {

    isCallingService = true;
    return await repository!.login(userName, password).then((apiResponse) {
      isCallingService = false;
      return apiResponse;
    });
  }


}
