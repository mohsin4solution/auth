import 'package:auth/auth/routes/AuthAppPagesConstants.dart';
import 'package:auth/auth/screens/login/UserViewModel.dart';
import 'package:auth/auth/utils/AppUtiles.dart';
import 'package:auth/auth/widgets/buttons/CustomRaisedButton.dart';
import 'package:auth/auth/widgets/texts/CustomTextSL.dart';
import 'package:flutter/material.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:network/HttpService.dart';
import 'package:network/SharedPrefManager.dart';
import 'package:network/models/Token.dart';
import 'package:network/models/theme/ThemeProvider.dart';
import 'package:network/models/user.dart';
import 'package:provider/provider.dart';

class LoginScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return
        ChangeNotifierProvider<UserViewModel>(
            create: (_) => UserViewModel(), child: _inerLoginScreen());
  }
}

class _inerLoginScreen extends StatefulWidget {
  @override
  __inerLoginScreenState createState() => __inerLoginScreenState();
}

class __inerLoginScreenState extends State<_inerLoginScreen> {
  GlobalKey<FormState> formKey = GlobalKey<FormState>();
  String? userName, password;
  User user = new User();

  @override
  Widget build(BuildContext context) {
    final themeProvider = Provider.of<ThemeProvider>(context);

    UserViewModel _provider = context.watch<UserViewModel>();

    return Scaffold(
      appBar: AppBar(
        title: Text("Login"),
        centerTitle: true,
      ),
      body: GestureDetector(
        onTap: () {
          FocusScope.of(context).unfocus();
        },
        child: Center(
          child: Container(
            margin: EdgeInsets.symmetric(vertical: 8, horizontal: 12),
            padding: EdgeInsets.symmetric(vertical: 30, horizontal: 16),
            width: AppUtiles.isInRow(context) ? 450.0 : double.infinity,
            decoration: BoxDecoration(
                color: themeProvider.themeMode().backgroundColor,
                borderRadius: BorderRadius.circular(15.0),
                boxShadow: [
                  BoxShadow(
                    color: Colors.black26,
                    blurRadius: 6.0,
                    offset: Offset(0.0, 2.0),
                  )
                ]),
            child: SingleChildScrollView(
              child: Form(
                key: formKey,
                autovalidateMode: AutovalidateMode.always,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Text(
                      _provider.isCallingService ? "Service Calling" : "Login",
                      style: TextStyle(
                          fontSize: 25.0, fontWeight: FontWeight.w600),
                    ),
                    SizedBox(height: 40),
                    TextFormField(
                      initialValue: "adeel@gmail.com",
                      decoration: InputDecoration(
                        hintText: ("Enter name"),
                        labelText: "Name",
                        focusColor: themeProvider.themeData().accentColor,
                        prefixIcon: Icon(Icons.person,
                            color: Theme.of(context).primaryColor),
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(10.0)),
                        ),
                      ),
                      validator: (String? value) {
                        if (value!.isEmpty) {
                          return "Please enter name";
                        }
                        return null;
                      },
                      onSaved: (String? value) {
                        setState(() {
                          userName = value;
                        });
                      },
                    ),
                    SizedBox(height: 8),
                    TextFormField(
                      // password
                      initialValue: "Ma.123",
                      keyboardType: TextInputType.text,
                      obscureText: true,
                      decoration: InputDecoration(
                          hintText: ("Enter password"),
                          labelText: "Password",
                          focusColor: Colors.blue,
                          prefixIcon: Icon(Icons.lock,
                              color: Theme.of(context).primaryColor),
                          border: OutlineInputBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(10.0)),
                          )),

                      validator: MultiValidator([
                        RequiredValidator(errorText: "Enter password"),
                        MinLengthValidator(2,
                            errorText:
                                'password must be at least 2 digits long'),
                      ]),
                      onChanged: (value) {
                        password = value;
                      },
                      onSaved: (String? value) {
                        setState(() {
                          password = value;
                        });
                      },
                    ),
                    // confirm password
                    SizedBox(height: 20),

                    CustomRaisedButton(
                      color: Theme.of(context).primaryColor,
                      onPress: () {
                        if (formKey.currentState!.validate()) {
                          formKey.currentState!.save();

                          _provider.login(userName, password).then((value) {
                            if (!value.error) {
                              Token? token = value.data;
                              HttpService.token = token;
                              AppUtiles.showSnackBar(
                                  context, "Login successflully.");

                              user.name = userName;
                              user.password = password;
                              user.token = token;
                              SharedPrefManagerUser.getInstance().then((pref) {
                                pref!.saveUser(user);
                                Navigator.pushReplacementNamed(
                                    context,
                                    AuthAppPagesConstants
                                        .mainScreenBMSProPageRoute);
                              });
                            } else {
                              AppUtiles.showSnackBar(
                                  context, value.errorMessage);
                            }
                          });
                        }
                      },
                      child: CustomTextSL(
                        text: "Login",
                        color: Colors.white,
                      ),
                    ),
                    SizedBox(height: 20),
                    GestureDetector(
                      onTap: () {
                        Navigator.pushNamed(
                            context, AuthAppPagesConstants.registerPageRoute);
                      },
                      child: Center(
                        child: CustomTextSL(
                          text: "Register Now",
                          color: Colors.black,
                        ),
                      ),
                    ),

                    //////////// temp remove this
                    SizedBox(height: 20),
                    GestureDetector(
                      onTap: () {
                        Navigator.pushNamed(
                            context, AuthAppPagesConstants.tempLangPageRoute);
                      },
                      child: Center(
                        child: CustomTextSL(
                          text: "Languages",
                          color: Colors.black,
                        ),
                      ),
                    ),

                    SizedBox(height: 20),
                  ],
                ),
              ),
              // ],
            ),
          ),
        ),
      ),
    );
  }
}
