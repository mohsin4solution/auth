
import 'package:auth/auth/screens/register/registerViewModel.dart';
import 'package:auth/auth/utils/AppUtiles.dart';
import 'package:auth/auth/widgets/buttons/CustomRaisedButton.dart';
import 'package:auth/auth/widgets/texts/CustomTextSL.dart';
import 'package:flutter/material.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:provider/provider.dart';

class RegisterScreen extends StatefulWidget {
  RegisterScreen({Key? key}) : super(key: key);

  @override
  _RegisterScreenState createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  @override
  Widget build(BuildContext context) {
    return  Scaffold(
        appBar: AppBar(
          title: Text("Register"),
          centerTitle: true,
        ),
        // body: _inerLoginScreen(),
        body: ChangeNotifierProvider<RegisterViewModel>(
            create: (_) => RegisterViewModel(), child: _inerRegisterScreen()),

    );
  }
}

class _inerRegisterScreen extends StatefulWidget {
  _inerRegisterScreen({Key? key}) : super(key: key);

  @override
  __inerRegisterScreenState createState() => __inerRegisterScreenState();
}

class __inerRegisterScreenState extends State<_inerRegisterScreen> {
  GlobalKey<FormState> formKey = GlobalKey<FormState>();
  String? name, userName, password, email;

  @override
  Widget build(BuildContext context) {
    // UserViewModel _provider = context.watch<UserViewModel>();
    RegisterViewModel _provider = context.watch<RegisterViewModel>();
    return Container(
      child: Center(
        child: Container(
          margin: EdgeInsets.symmetric(vertical: 8, horizontal: 12),
          padding: EdgeInsets.symmetric(vertical: 30, horizontal: 16),
          width: AppUtiles.isInRow(context) ? 450.0 : double.infinity,
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(15.0),
              boxShadow: [
                BoxShadow(
                  color: Colors.black26,
                  blurRadius: 6.0,
                  offset: Offset(0.0, 2.0),
                )
              ]),
          child: SingleChildScrollView(
            child: Form(
              key: formKey,
              autovalidateMode: AutovalidateMode.always,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Text(
                    _provider.isCallingService ? "Service Calling" : "Register",
                    style:
                        TextStyle(fontSize: 25.0, fontWeight: FontWeight.w600),
                  ),
                  SizedBox(height: 40),

                  TextFormField(
                    decoration: InputDecoration(
                      hintText: ("Enter name"),
                      labelText: "Name",
                      focusColor: Colors.blue,
                      prefixIcon: Icon(Icons.person,
                          color: Theme.of(context).primaryColor),
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(10.0)),
                      ),
                    ),
                    validator: (String? value) {
                      if (value!.isEmpty) {
                        return "Please enter name";
                      }
                      return null;
                    },
                    onSaved: (String? value) {
                      setState(() {
                        name = value;
                      });
                    },
                  ),

                  SizedBox(height: 8),
                  TextFormField(
                    decoration: InputDecoration(
                      hintText: ("Enter user name"),
                      labelText: "User Name",
                      focusColor: Colors.blue,
                      prefixIcon: Icon(Icons.person,
                          color: Theme.of(context).primaryColor),
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(10.0)),
                      ),
                    ),
                    validator: (String? value) {
                      if (value!.isEmpty) {
                        return "Please enter user name";
                      }
                      return null;
                    },
                    onSaved: (String? value) {
                      setState(() {
                        userName = value;
                      });
                    },
                  ),

                  SizedBox(height: 8),
                  TextFormField(
                    // password
                    keyboardType: TextInputType.text,
                    obscureText: true,
                    decoration: InputDecoration(
                        hintText: ("Enter password"),
                        labelText: "Password",
                        focusColor: Colors.blue,
                        prefixIcon: Icon(Icons.lock,
                            color: Theme.of(context).primaryColor),
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(10.0)),
                        )),

                    validator: MultiValidator([
                      RequiredValidator(errorText: "Enter password"),
                      MinLengthValidator(2,
                          errorText: 'password must be at least 2 digits long'),
                    ]) ,
                    onChanged: (value) {
                      password = value;
                    },
                    onSaved: (String? value) {
                      setState(() {
                        password = value;
                      });
                    },
                  ),

                  SizedBox(height: 8),
                  TextFormField(
                    decoration: InputDecoration(
                      hintText: ("Enter user email"),
                      labelText: "email",
                      focusColor: Colors.blue,
                      prefixIcon: Icon(Icons.email,
                          color: Theme.of(context).primaryColor),
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(10.0)),
                      ),
                    ),
                    validator: (String? value) {
                      if (value!.isEmpty) {
                        return "Please enter email";
                      }
                      return null;
                    },
                    onSaved: (String? value) {
                      setState(() {
                        email = value;
                      });
                    },
                  ),

                  SizedBox(height: 20),
                  CustomRaisedButton(
                    color: Theme.of(context).primaryColor,
                    onPress: () {
                      if (formKey.currentState!.validate()) {
                        formKey.currentState!.save();

                        _provider
                            .register(email , password,  "3008097654",  userName,name , 1, "1998-05-20")
                            .then(
                          (value) {
                            if (!value.error) {
                              AppUtiles.showSnackBar(
                                  context, "Register successflully.");

                            } else {
                              AppUtiles.showSnackBar(
                                  context, value.errorMessage);
                            }
                          },
                        );
                      }
                    },
                    child: CustomTextSL(
                      text: "Register",
                      color: Colors.white,
                    ),
                  ),

                  SizedBox(height: 20),
                  GestureDetector(
                    onTap: () {
                      Navigator.pop(context);
                    },
                    child: Center(
                      child: CustomTextSL(
                        text: "Back to login?",
                        color: Colors.black,
                      ),
                    ),
                  ),
                  SizedBox(height: 20),
                ],
              ),
            ),
            // ],
          ),
        ),
      ),
    );
  }
}
