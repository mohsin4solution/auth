import 'package:auth/auth/repo/AuthRepo.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:network/HttpService.dart';

class RegisterViewModel extends ChangeNotifier {
  RegisterViewModel();

  bool _isError = false;
  bool _isData = true;
  bool _isCallingService = false;
  String? _errorMessage;

  AuthRepo? get repository => GetIt.I<AuthRepo>();

  bool get isError => _isError;

  set isError(bool value) {
    _isError = value;
  }

  bool get isData => _isData;

  set isData(bool value) {
    _isData = value;
  }

  bool get isCallingService => _isCallingService;

  set isCallingService(bool value) {
    _isCallingService = value;
    notifyListeners();
  }

  String? get errorMessage => _errorMessage;

  set errorMessage(String? value) {
    _errorMessage = value;
  }



  Future<APIRespons> register(
      String? email, String? password, String? phoneNumber,
      String? userName, String? name , int gender, String dob) async {
    isCallingService = true;
    return await repository!
        .register( email,  password,  phoneNumber, userName,  name ,  gender,  dob)
        .then((apiResponse) {
      isCallingService = false;
      return apiResponse;
    });
  }
}
