import 'package:auth/auth/screens/lang/LanguagesViewModel.dart';
import 'package:auth/auth/screens/lang/languages_model.dart';

import 'package:auth/auth/screens/login/UserViewModel.dart';
import 'package:auth/auth/utils/AppUtiles.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';



class AllLanguages extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(

        home: ChangeNotifierProvider<LanguagesViewModel>(
            create: (_) => LanguagesViewModel(), child: _inerLanguage()),

    );
  }
}

LanguagesViewModel? _languagesViewModel = null;

class _inerLanguage extends StatefulWidget {
  @override
  __inerLanguageState createState() => __inerLanguageState();
}

class __inerLanguageState extends State<_inerLanguage> {


  @override
  Widget build(BuildContext context) {

     _languagesViewModel = context.watch<LanguagesViewModel>();

    return Scaffold(
      appBar: AppBar(
        title: Text("Login"),
        centerTitle: true,
        actions: [
          GestureDetector(
            onTap: (){

              if(_languagesViewModel!=null) {
                _languagesViewModel!.addLanguage("badffff").then((value) {
                  if(value.error){
                    AppUtiles.showSnackBar(context, value.errorMessage);
                    print(value.errorMessage);
                  }
                });
              }
            },
            child: Icon(Icons.add, size: 30),
          )
        ],
      ),
      body: Builder(
        builder: (BuildContext context) {
          Widget widget ;
          if(_languagesViewModel!.isError){
            widget  = GestureDetector(
                onTap: (){

                  _languagesViewModel!.getAllLanguages();
                },
                child: Container( width: double.infinity, height: double.infinity, child: Center(child: Text("Error, \n please tap to retry again"))));
          }else {

            widget = Stack(
              children: [
                  Container(
                  child: ListView.builder(
                      itemCount: _languagesViewModel!.languages?.data?.allLanguages?.languages?.length??0,
                      itemBuilder:  (BuildContext ctxt, int index) {

                        List<Languages> list =  _languagesViewModel!.languages!.data!.allLanguages!.languages!;
                        return Row(
                          children: [
                            Expanded(child: Text(list[index].languageName!)),
                            GestureDetector(
                              onTap: (){

                                _languagesViewModel!.deleteLanguage(list[index].id).then((value) {
                                  if(value.error){
                                    AppUtiles.showSnackBar(context, value.errorMessage);
                                  }
                                });
                              },
                              child: Icon(Icons.delete, size: 30),
                            ),
                            GestureDetector(
                              onTap: (){

                                _languagesViewModel!.updateLanguage(list[index].id, "yyggg").then((value) {

                                  if(value.error){
                                    AppUtiles.showSnackBar(context, value.errorMessage);
                                  }
                                });
                              },
                              child: Icon(Icons.edit, size: 30),
                            )

                          ],
                        );

                      }

                  ),

                ),
                _languagesViewModel!.isCallingService ? SizedBox( width: double.infinity, height: double.infinity,   child: AbsorbPointer( absorbing: true, child: Center(child: CircularProgressIndicator()))) : SizedBox.shrink(),

              ],

            );

          }

          return widget;

        }

      ),
    );
  }
}

