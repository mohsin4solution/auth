/// data : {"allLanguages":{"languages":[{"id":"3","languageName":"Arabic"},{"id":"4","languageName":"English"},{"id":"5","languageName":"Chinese"},{"id":"6","languageName":"Spanish"},{"id":"7","languageName":"Hindi"},{"id":"8","languageName":"Portuguese"},{"id":"9","languageName":"Bengali"},{"id":"10","languageName":"Russian"},{"id":"11","languageName":"Japanese"},{"id":"12","languageName":"Punjabi / Lahnda"},{"id":"13","languageName":"yyyy"},{"id":"14","languageName":"kkkk"},{"id":"15","languageName":"lllll"},{"id":"16","languageName":"nnn"},{"id":"17","languageName":"mmmmm"},{"id":"18","languageName":"llfd"},{"id":"19","languageName":"mlmlmlm"},{"id":"20","languageName":"nnnnnll"},{"id":"21","languageName":"pppppp"},{"id":"22","languageName":"mehdi"},{"id":"24","languageName":"kkkkkddd"},{"id":"25","languageName":"zedkljnzked"},{"id":"26","languageName":"zdklnklzc"},{"id":"27","languageName":"farsi"},{"id":"29","languageName":"punjabi/lahore"},{"id":"30","languageName":"Pashto"},{"id":"33","languageName":"Flutter"},{"id":"34","languageName":"Amazigh"},{"id":"35","languageName":"Frensh"},{"id":"39","languageName":"Mandarin Chinese"},{"id":"42","languageName":"Hindi (sanskritised Hindustani)"},{"id":"47","languageName":"Western Punjabi"},{"id":"48","languageName":"Marathi"},{"id":"49","languageName":"Telugu"},{"id":"50","languageName":"Wu Chinese"},{"id":"51","languageName":"Turkish"},{"id":"52","languageName":"Korean"},{"id":"53","languageName":"French"},{"id":"54","languageName":"German (only Standard German)"},{"id":"55","languageName":"Vietnamese"},{"id":"56","languageName":"Tamil"},{"id":"57","languageName":"Yue Chinese"},{"id":"58","languageName":"Urdu (persianised Hindustani)"},{"id":"59","languageName":"Javanese"},{"id":"60","languageName":"Italian"},{"id":"62","languageName":"Egyptian Arabic"},{"id":"63","languageName":"Gujarati"},{"id":"64","languageName":"Iranian Persian"},{"id":"65","languageName":"Bhojpuri"},{"id":"66","languageName":"Min Nan Chinese"},{"id":"67","languageName":"Hakka Chinese"},{"id":"68","languageName":"Jin Chinese"},{"id":"69","languageName":"Hausa"},{"id":"70","languageName":"Kannada"},{"id":"71","languageName":"Indonesian (Indonesian Malay)"},{"id":"72","languageName":"Polish"},{"id":"73","languageName":"Yoruba"},{"id":"74","languageName":"Xiang Chinese"},{"id":"75","languageName":"Malayalam"},{"id":"76","languageName":"Odia"},{"id":"77","languageName":"Maithili"},{"id":"78","languageName":"Burmese"},{"id":"79","languageName":"Eastern Punjabi"},{"id":"80","languageName":"Sunda"},{"id":"81","languageName":"Sudanese Arabic"},{"id":"82","languageName":"Algerian Arabic"},{"id":"83","languageName":"Moroccan Arabic"},{"id":"84","languageName":"Ukrainian"},{"id":"85","languageName":"Igbo"},{"id":"86","languageName":"Northern Uzbek"},{"id":"87","languageName":"Sindhi"},{"id":"88","languageName":"North Levantine Arabic"},{"id":"89","languageName":"Romanian"},{"id":"90","languageName":"Tagalog"},{"id":"91","languageName":"Dutch"},{"id":"92","languageName":"Saʽidi Arabic"},{"id":"93","languageName":"Gan Chinese"},{"id":"94","languageName":"Amharic"},{"id":"95","languageName":"Northern Pashto"},{"id":"96","languageName":"Magahi"},{"id":"97","languageName":"Thai"},{"id":"98","languageName":"Saraiki"},{"id":"99","languageName":"Khmer"},{"id":"100","languageName":"Chhattisgarhi"},{"id":"101","languageName":"Somali"},{"id":"102","languageName":"Malaysian (Malaysian Malay)"},{"id":"103","languageName":"Cebuano"},{"id":"104","languageName":"Nepali"},{"id":"105","languageName":"Mesopotamian Arabic"},{"id":"106","languageName":"Assamese"},{"id":"107","languageName":"Sinhalese"},{"id":"108","languageName":"Northern Kurdish"},{"id":"109","languageName":"Hejazi Arabic"},{"id":"110","languageName":"Nigerian Fulfulde"},{"id":"111","languageName":"Bavarian"},{"id":"112","languageName":"South Azerbaijani"},{"id":"113","languageName":"Greek"},{"id":"114","languageName":"Chittagonian"},{"id":"115","languageName":"Kazakh"},{"id":"116","languageName":"Deccan"},{"id":"117","languageName":"Hungarian"},{"id":"118","languageName":"Kinyarwanda"},{"id":"119","languageName":"Zulu"},{"id":"120","languageName":"South Levantine Arabic"},{"id":"121","languageName":"Tunisian Arabic"},{"id":"122","languageName":"Sanaani Spoken Arabic"},{"id":"123","languageName":"Min Bei Chinese"},{"id":"124","languageName":"Southern Pashto"},{"id":"125","languageName":"Rundi"},{"id":"126","languageName":"Czech"},{"id":"127","languageName":"Taʽizzi-Adeni Arabic"},{"id":"128","languageName":"Uyghur"},{"id":"129","languageName":"Min Dong Chinese"},{"id":"130","languageName":"Sylheti"},{"id":"132","languageName":"Standard Arabic"},{"id":"133","languageName":"Urdu"},{"id":"134","languageName":"Indonesian"},{"id":"135","languageName":"Standard German"},{"id":"136","languageName":"xx"}]}}

class LanguagesModel {
  Data? _data;

  Data? get data => _data;

  LanguagesModel({
      Data? data}){
    _data = data;
}

  LanguagesModel.fromJson(dynamic json) {
    _data = json["data"] != null ? Data.fromJson(json["data"]) : null;
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    if (_data != null) {
      map["data"] = _data!.toJson();
    }
    return map;
  }

}

/// allLanguages : {"languages":[{"id":"3","languageName":"Arabic"},{"id":"4","languageName":"English"},{"id":"5","languageName":"Chinese"},{"id":"6","languageName":"Spanish"},{"id":"7","languageName":"Hindi"},{"id":"8","languageName":"Portuguese"},{"id":"9","languageName":"Bengali"},{"id":"10","languageName":"Russian"},{"id":"11","languageName":"Japanese"},{"id":"12","languageName":"Punjabi / Lahnda"},{"id":"13","languageName":"yyyy"},{"id":"14","languageName":"kkkk"},{"id":"15","languageName":"lllll"},{"id":"16","languageName":"nnn"},{"id":"17","languageName":"mmmmm"},{"id":"18","languageName":"llfd"},{"id":"19","languageName":"mlmlmlm"},{"id":"20","languageName":"nnnnnll"},{"id":"21","languageName":"pppppp"},{"id":"22","languageName":"mehdi"},{"id":"24","languageName":"kkkkkddd"},{"id":"25","languageName":"zedkljnzked"},{"id":"26","languageName":"zdklnklzc"},{"id":"27","languageName":"farsi"},{"id":"29","languageName":"punjabi/lahore"},{"id":"30","languageName":"Pashto"},{"id":"33","languageName":"Flutter"},{"id":"34","languageName":"Amazigh"},{"id":"35","languageName":"Frensh"},{"id":"39","languageName":"Mandarin Chinese"},{"id":"42","languageName":"Hindi (sanskritised Hindustani)"},{"id":"47","languageName":"Western Punjabi"},{"id":"48","languageName":"Marathi"},{"id":"49","languageName":"Telugu"},{"id":"50","languageName":"Wu Chinese"},{"id":"51","languageName":"Turkish"},{"id":"52","languageName":"Korean"},{"id":"53","languageName":"French"},{"id":"54","languageName":"German (only Standard German)"},{"id":"55","languageName":"Vietnamese"},{"id":"56","languageName":"Tamil"},{"id":"57","languageName":"Yue Chinese"},{"id":"58","languageName":"Urdu (persianised Hindustani)"},{"id":"59","languageName":"Javanese"},{"id":"60","languageName":"Italian"},{"id":"62","languageName":"Egyptian Arabic"},{"id":"63","languageName":"Gujarati"},{"id":"64","languageName":"Iranian Persian"},{"id":"65","languageName":"Bhojpuri"},{"id":"66","languageName":"Min Nan Chinese"},{"id":"67","languageName":"Hakka Chinese"},{"id":"68","languageName":"Jin Chinese"},{"id":"69","languageName":"Hausa"},{"id":"70","languageName":"Kannada"},{"id":"71","languageName":"Indonesian (Indonesian Malay)"},{"id":"72","languageName":"Polish"},{"id":"73","languageName":"Yoruba"},{"id":"74","languageName":"Xiang Chinese"},{"id":"75","languageName":"Malayalam"},{"id":"76","languageName":"Odia"},{"id":"77","languageName":"Maithili"},{"id":"78","languageName":"Burmese"},{"id":"79","languageName":"Eastern Punjabi"},{"id":"80","languageName":"Sunda"},{"id":"81","languageName":"Sudanese Arabic"},{"id":"82","languageName":"Algerian Arabic"},{"id":"83","languageName":"Moroccan Arabic"},{"id":"84","languageName":"Ukrainian"},{"id":"85","languageName":"Igbo"},{"id":"86","languageName":"Northern Uzbek"},{"id":"87","languageName":"Sindhi"},{"id":"88","languageName":"North Levantine Arabic"},{"id":"89","languageName":"Romanian"},{"id":"90","languageName":"Tagalog"},{"id":"91","languageName":"Dutch"},{"id":"92","languageName":"Saʽidi Arabic"},{"id":"93","languageName":"Gan Chinese"},{"id":"94","languageName":"Amharic"},{"id":"95","languageName":"Northern Pashto"},{"id":"96","languageName":"Magahi"},{"id":"97","languageName":"Thai"},{"id":"98","languageName":"Saraiki"},{"id":"99","languageName":"Khmer"},{"id":"100","languageName":"Chhattisgarhi"},{"id":"101","languageName":"Somali"},{"id":"102","languageName":"Malaysian (Malaysian Malay)"},{"id":"103","languageName":"Cebuano"},{"id":"104","languageName":"Nepali"},{"id":"105","languageName":"Mesopotamian Arabic"},{"id":"106","languageName":"Assamese"},{"id":"107","languageName":"Sinhalese"},{"id":"108","languageName":"Northern Kurdish"},{"id":"109","languageName":"Hejazi Arabic"},{"id":"110","languageName":"Nigerian Fulfulde"},{"id":"111","languageName":"Bavarian"},{"id":"112","languageName":"South Azerbaijani"},{"id":"113","languageName":"Greek"},{"id":"114","languageName":"Chittagonian"},{"id":"115","languageName":"Kazakh"},{"id":"116","languageName":"Deccan"},{"id":"117","languageName":"Hungarian"},{"id":"118","languageName":"Kinyarwanda"},{"id":"119","languageName":"Zulu"},{"id":"120","languageName":"South Levantine Arabic"},{"id":"121","languageName":"Tunisian Arabic"},{"id":"122","languageName":"Sanaani Spoken Arabic"},{"id":"123","languageName":"Min Bei Chinese"},{"id":"124","languageName":"Southern Pashto"},{"id":"125","languageName":"Rundi"},{"id":"126","languageName":"Czech"},{"id":"127","languageName":"Taʽizzi-Adeni Arabic"},{"id":"128","languageName":"Uyghur"},{"id":"129","languageName":"Min Dong Chinese"},{"id":"130","languageName":"Sylheti"},{"id":"132","languageName":"Standard Arabic"},{"id":"133","languageName":"Urdu"},{"id":"134","languageName":"Indonesian"},{"id":"135","languageName":"Standard German"},{"id":"136","languageName":"xx"}]}

class Data {
  AllLanguages? _allLanguages;

  AllLanguages? get allLanguages => _allLanguages;

  Data({
      AllLanguages? allLanguages}){
    _allLanguages = allLanguages;
}

  Data.fromJson(dynamic json) {
    _allLanguages = json["allLanguages"] != null ? AllLanguages.fromJson(json["allLanguages"]) : null;
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    if (_allLanguages != null) {
      map["allLanguages"] = _allLanguages!.toJson();
    }
    return map;
  }

}

/// languages : [{"id":"3","languageName":"Arabic"},{"id":"4","languageName":"English"},{"id":"5","languageName":"Chinese"},{"id":"6","languageName":"Spanish"},{"id":"7","languageName":"Hindi"},{"id":"8","languageName":"Portuguese"},{"id":"9","languageName":"Bengali"},{"id":"10","languageName":"Russian"},{"id":"11","languageName":"Japanese"},{"id":"12","languageName":"Punjabi / Lahnda"},{"id":"13","languageName":"yyyy"},{"id":"14","languageName":"kkkk"},{"id":"15","languageName":"lllll"},{"id":"16","languageName":"nnn"},{"id":"17","languageName":"mmmmm"},{"id":"18","languageName":"llfd"},{"id":"19","languageName":"mlmlmlm"},{"id":"20","languageName":"nnnnnll"},{"id":"21","languageName":"pppppp"},{"id":"22","languageName":"mehdi"},{"id":"24","languageName":"kkkkkddd"},{"id":"25","languageName":"zedkljnzked"},{"id":"26","languageName":"zdklnklzc"},{"id":"27","languageName":"farsi"},{"id":"29","languageName":"punjabi/lahore"},{"id":"30","languageName":"Pashto"},{"id":"33","languageName":"Flutter"},{"id":"34","languageName":"Amazigh"},{"id":"35","languageName":"Frensh"},{"id":"39","languageName":"Mandarin Chinese"},{"id":"42","languageName":"Hindi (sanskritised Hindustani)"},{"id":"47","languageName":"Western Punjabi"},{"id":"48","languageName":"Marathi"},{"id":"49","languageName":"Telugu"},{"id":"50","languageName":"Wu Chinese"},{"id":"51","languageName":"Turkish"},{"id":"52","languageName":"Korean"},{"id":"53","languageName":"French"},{"id":"54","languageName":"German (only Standard German)"},{"id":"55","languageName":"Vietnamese"},{"id":"56","languageName":"Tamil"},{"id":"57","languageName":"Yue Chinese"},{"id":"58","languageName":"Urdu (persianised Hindustani)"},{"id":"59","languageName":"Javanese"},{"id":"60","languageName":"Italian"},{"id":"62","languageName":"Egyptian Arabic"},{"id":"63","languageName":"Gujarati"},{"id":"64","languageName":"Iranian Persian"},{"id":"65","languageName":"Bhojpuri"},{"id":"66","languageName":"Min Nan Chinese"},{"id":"67","languageName":"Hakka Chinese"},{"id":"68","languageName":"Jin Chinese"},{"id":"69","languageName":"Hausa"},{"id":"70","languageName":"Kannada"},{"id":"71","languageName":"Indonesian (Indonesian Malay)"},{"id":"72","languageName":"Polish"},{"id":"73","languageName":"Yoruba"},{"id":"74","languageName":"Xiang Chinese"},{"id":"75","languageName":"Malayalam"},{"id":"76","languageName":"Odia"},{"id":"77","languageName":"Maithili"},{"id":"78","languageName":"Burmese"},{"id":"79","languageName":"Eastern Punjabi"},{"id":"80","languageName":"Sunda"},{"id":"81","languageName":"Sudanese Arabic"},{"id":"82","languageName":"Algerian Arabic"},{"id":"83","languageName":"Moroccan Arabic"},{"id":"84","languageName":"Ukrainian"},{"id":"85","languageName":"Igbo"},{"id":"86","languageName":"Northern Uzbek"},{"id":"87","languageName":"Sindhi"},{"id":"88","languageName":"North Levantine Arabic"},{"id":"89","languageName":"Romanian"},{"id":"90","languageName":"Tagalog"},{"id":"91","languageName":"Dutch"},{"id":"92","languageName":"Saʽidi Arabic"},{"id":"93","languageName":"Gan Chinese"},{"id":"94","languageName":"Amharic"},{"id":"95","languageName":"Northern Pashto"},{"id":"96","languageName":"Magahi"},{"id":"97","languageName":"Thai"},{"id":"98","languageName":"Saraiki"},{"id":"99","languageName":"Khmer"},{"id":"100","languageName":"Chhattisgarhi"},{"id":"101","languageName":"Somali"},{"id":"102","languageName":"Malaysian (Malaysian Malay)"},{"id":"103","languageName":"Cebuano"},{"id":"104","languageName":"Nepali"},{"id":"105","languageName":"Mesopotamian Arabic"},{"id":"106","languageName":"Assamese"},{"id":"107","languageName":"Sinhalese"},{"id":"108","languageName":"Northern Kurdish"},{"id":"109","languageName":"Hejazi Arabic"},{"id":"110","languageName":"Nigerian Fulfulde"},{"id":"111","languageName":"Bavarian"},{"id":"112","languageName":"South Azerbaijani"},{"id":"113","languageName":"Greek"},{"id":"114","languageName":"Chittagonian"},{"id":"115","languageName":"Kazakh"},{"id":"116","languageName":"Deccan"},{"id":"117","languageName":"Hungarian"},{"id":"118","languageName":"Kinyarwanda"},{"id":"119","languageName":"Zulu"},{"id":"120","languageName":"South Levantine Arabic"},{"id":"121","languageName":"Tunisian Arabic"},{"id":"122","languageName":"Sanaani Spoken Arabic"},{"id":"123","languageName":"Min Bei Chinese"},{"id":"124","languageName":"Southern Pashto"},{"id":"125","languageName":"Rundi"},{"id":"126","languageName":"Czech"},{"id":"127","languageName":"Taʽizzi-Adeni Arabic"},{"id":"128","languageName":"Uyghur"},{"id":"129","languageName":"Min Dong Chinese"},{"id":"130","languageName":"Sylheti"},{"id":"132","languageName":"Standard Arabic"},{"id":"133","languageName":"Urdu"},{"id":"134","languageName":"Indonesian"},{"id":"135","languageName":"Standard German"},{"id":"136","languageName":"xx"}]

class AllLanguages {
  List<Languages>? _languages;

  List<Languages>? get languages => _languages;

  AllLanguages({
      List<Languages>? languages}){
    _languages = languages;
}

  AllLanguages.fromJson(dynamic json) {
    if (json["languages"] != null) {
      _languages = [];
      json["languages"].forEach((v) {
        _languages!.add(Languages.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    if (_languages != null) {
      map["languages"] = _languages!.map((v) => v.toJson()).toList();
    }
    return map;
  }

}

/// id : "3"
/// languageName : "Arabic"

class Languages {
  String? _id;
  String? _languageName;

  String? get id => _id;
  String? get languageName => _languageName;

  Languages({
      String? id, 
      String? languageName}){
    _id = id;
    _languageName = languageName;
}

  Languages.fromJson(dynamic json) {
    _id = json["id"];
    _languageName = json["languageName"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["id"] = _id;
    map["languageName"] = _languageName;
    return map;
  }

}