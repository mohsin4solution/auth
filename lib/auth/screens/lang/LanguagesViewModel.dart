import 'package:auth/auth/repo/AuthRepo.dart';
import 'package:auth/auth/screens/lang/insert_language.dart';
import 'package:auth/auth/screens/lang/languages_model.dart';
import 'package:auth/auth/screens/lang/update_languge.dart';
import 'package:auth/auth/utils/AppUtiles.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:network/HttpService.dart';

class LanguagesViewModel extends ChangeNotifier {

  LanguagesViewModel(){
    getAllLanguages();
  }


  LanguagesModel? languages = LanguagesModel();



  bool _isError = false;
  bool _isData  = true;
  bool _isCallingService = false;
  String? _errorMessage;

  AuthRepo? get repository => GetIt.I<AuthRepo>();

  bool get isError => _isError;

  set isError(bool value) {
    _isError = value;
  }

  bool get isData => _isData;

  set isData(bool value) {
    _isData = value;
  }

  bool get isCallingService => _isCallingService;

  set isCallingService(bool value) {
    _isCallingService = value;
    notifyListeners();
  }

  String? get errorMessage => _errorMessage;

  set errorMessage(String? value) {
    _errorMessage = value;
  }

  void getAllLanguages()  {

    isCallingService = true;
    _isError = false;
    _isData = false;
     repository!.getLanguages().then((apiResponse) {
      isCallingService = false;
      if(!apiResponse.error){
        _isData = true;
        languages = apiResponse.data;

      }else{
        _isError = true;
        _errorMessage = apiResponse.errorMessage;
      }
      notifyListeners();

    });
  }


  Future<APIRespons> deleteLanguage (String? id) async{

    isCallingService = true;

    return await   repository!.deleteLanguage(id).then((apiResponse) async {
      isCallingService = false;
      if(!apiResponse.error){
        languages?.data?.allLanguages?.languages?.forEach((element) {
          if(element.id == id){
            languages!.data!.allLanguages!.languages!.remove(element);

            notifyListeners();
            return;
          }
        });
      }
      return apiResponse;
    });
  }





  Future<APIRespons> addLanguage (String langName) async{

    isCallingService = true;

    return await repository!.addLanguage(langName).then((apiResponse) async {
      isCallingService = false;
      if(!apiResponse.error){

        InsertLanguages languagesObj = apiResponse.data;
        Languages languagesAdd = Languages(languageName: languagesObj.languageName, id: languagesObj.id);
        languages?.data?.allLanguages?.languages?.add(languagesAdd);
        notifyListeners();
      }
      return apiResponse;
    });
  }


  Future<APIRespons> updateLanguage (String? id, String updatedName) async{

    isCallingService = true;

    return await repository!.updateLanguage(id, updatedName).then((apiResponse) async {
      isCallingService = false;
      if(!apiResponse.error){

        UpdateLanguages languagesObj = apiResponse.data;
        Languages languagesAdd = Languages(languageName: languagesObj.languageName, id: languagesObj.id);


        for(int i=0; i<languages!.data!.allLanguages!.languages!.length; i++){
          if(languages?.data?.allLanguages?.languages![i].id==languagesAdd.id){
            languages?.data?.allLanguages?.languages![i] = languagesAdd;
          }
        }
        notifyListeners();
      }
      return apiResponse;
    });
  }


}
