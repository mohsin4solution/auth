/// data : {"updateLanguages":[{"id":"176","languageName":"hhh"}]}

class UpdateLanguage {
  Data? _data;

  Data? get data => _data;

  UpdateLanguage({
      Data? data}){
    _data = data;
}

  UpdateLanguage.fromJson(dynamic json) {
    _data = json["data"] != null ? Data.fromJson(json["data"]) : null;
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    if (_data != null) {
      map["data"] = _data!.toJson();
    }
    return map;
  }

}

/// updateLanguages : [{"id":"176","languageName":"hhh"}]

class Data {
  List<UpdateLanguages>? _updateLanguages;

  List<UpdateLanguages>? get updateLanguages => _updateLanguages;

  Data({
      List<UpdateLanguages>? updateLanguages}){
    _updateLanguages = updateLanguages;
}

  Data.fromJson(dynamic json) {
    if (json["updateLanguages"] != null) {
      _updateLanguages = [];
      json["updateLanguages"].forEach((v) {
        _updateLanguages!.add(UpdateLanguages.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    if (_updateLanguages != null) {
      map["updateLanguages"] = _updateLanguages!.map((v) => v.toJson()).toList();
    }
    return map;
  }

}

/// id : "176"
/// languageName : "hhh"

class UpdateLanguages {
  String? _id;
  String? _languageName;

  String? get id => _id;
  String? get languageName => _languageName;

  UpdateLanguages({
      String? id, 
      String? languageName}){
    _id = id;
    _languageName = languageName;
}

  UpdateLanguages.fromJson(dynamic json) {
    _id = json["id"];
    _languageName = json["languageName"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["id"] = _id;
    map["languageName"] = _languageName;
    return map;
  }

}