/// data : {"insertLanguages":[{"id":"141","languageName":"sdfdf"}]}

class InsertLanguage {
  Data? _data;

  Data? get data => _data;

  InsertLanguage({
      Data? data}){
    _data = data;
}

  InsertLanguage.fromJson(dynamic json) {
    _data = json["data"] != null ? Data.fromJson(json["data"]) : null;
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    if (_data != null) {
      map["data"] = _data!.toJson();
    }
    return map;
  }

}

/// insertLanguages : [{"id":"141","languageName":"sdfdf"}]

class Data {
  List<InsertLanguages>? _insertLanguages;

  List<InsertLanguages>? get insertLanguages => _insertLanguages;

  Data({
      List<InsertLanguages>? insertLanguages}){
    _insertLanguages = insertLanguages;
}

  Data.fromJson(dynamic json) {
    if (json["insertLanguages"] != null) {
      _insertLanguages = [];
      json["insertLanguages"].forEach((v) {
        _insertLanguages!.add(InsertLanguages.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    if (_insertLanguages != null) {
      map["insertLanguages"] = _insertLanguages!.map((v) => v.toJson()).toList();
    }
    return map;
  }

}

/// id : "141"
/// languageName : "sdfdf"

class InsertLanguages {
  String? _id;
  String? _languageName;

  String? get id => _id;
  String? get languageName => _languageName;

  InsertLanguages({
      String? id, 
      String? languageName}){
    _id = id;
    _languageName = languageName;
}

  InsertLanguages.fromJson(dynamic json) {
    _id = json["id"];
    _languageName = json["languageName"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["id"] = _id;
    map["languageName"] = _languageName;
    return map;
  }

}