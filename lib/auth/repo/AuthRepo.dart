import 'dart:convert';

import 'package:auth/auth.dart';
import 'package:auth/auth/configuration/Constants.dart';
import 'package:auth/auth/screens/lang/insert_language.dart';
import 'package:auth/auth/screens/lang/languages_model.dart';
import 'package:auth/auth/screens/lang/update_languge.dart';
import 'package:network/HttpService.dart';
import 'package:network/models/Token.dart';

class AuthRepo {
  Future<APIRespons> login(String? userName, String? password) async {
    String ssoUrl = "${Constants.baseUrl}account/login?email=$userName&password=$password";
    // var body = {
    //   // "client_id": Constants.CLIENT_ID,
    //   // "client_secret": Constants.SECRET,
    //   // "grant_type": Constants.grant_type,
    //   // "scope": Constants.scope,
    //   "email": userName,
    //   "password": password
    // };

    // Map<String, String> header = {
    //   "Content-Type": "application/x-www-form-urlencoded"
    // };
    //
    // String contentType = "application/x-www-form-urlencoded";

    APIRequest apiRequest = APIRequest(
        url: ssoUrl,
        body: "",
        id: 1,
        method: Method.POST,);
    return await HttpService.instance.callService(apiRequest).then((result) {
      if (!result.error && result.data != null) {
        Token token = Token.fromJson(jsonDecode(result.data.toString()));
        result.data = token as Token;
      } else if (result.statusCode == 401) {
        result.errorMessage = "In Valid username or password,";
      }

      return result;
    });
  }

  Future<APIRespons> register(
      String? email, String? password, String? phoneNumber,
      String? userName, String? name , int gender, String dob) async {
    String ssoUrl = "${Constants.baseUrl}account/register";

    // {
    //   "Email" : "adeel@gmail.com",
    // "Password": "Ma.123",
    // "ConfirmPassword": "Ma.123",
    // "PhoneNumber": "+923008833553",
    // "UserName" : "adeel",
    // "Name" : "Adeel Zahid",
    // "gender" : 1,
    // "dob" : "2021-05-20"
    // }
    var body = {
      "Email": email,
      "Password": password,
      "ConfirmPassword": password,
      "PhoneNumber": phoneNumber,
      "UserName": userName,
      "Name": name,
      "gender": gender,
      "dob": dob
    };

    // Map<String, String> header = new Map<String, String>();
    // // header["Authorization"] = "Basic " + _getSSOAuthToken();
    // header["Content-Type"] = "application/json";

    APIRequest apiRequest = APIRequest(
        url: ssoUrl, body: body, id: 1,  method: Method.POST);
    return await HttpService.instance.callService(apiRequest).then((result) {
      if (!result.error && result.data != null) {
        var datajson = json.decode(result.data.toString());
        bool isSuccess = datajson["success"];
        if (isSuccess) {
          result.error = false;
          result.data = datajson["message"];
        } else {
          result.error = true;
          result.errorMessage = datajson["error"];
        }
      } else if (result.statusCode == 401) {
        result.errorMessage = "Email Address already exists,";
      } else if (result.statusCode == 403) {
        result.errorMessage = "Reegistration is not allowed,";
      }

      return result;
    });
  }

  static String _getSSOAuthToken() {
    String credentials = Constants.CLIENT_ID + ":" + Constants.SECRET;
    Codec<String, String> stringToBase64 = utf8.fuse(base64);
    String encoded = stringToBase64.encode(credentials);
    return encoded;
  }



  Future<APIRespons> getLanguages() async {
    String ssoUrl = "${Constants.baseUrl}graphql";

     String allLanguages= 'query allLanguages { allLanguages(offset :0, limit: 500) { languages { languageName id } } } ';
     var jsonBody = jsonEncode({"query": allLanguages});

    APIRequest apiRequest = APIRequest(
        url: ssoUrl,
        body: jsonBody);
    return await HttpService.instance.callService(apiRequest).then((result) {
      if (!result.error && result.data != null) {

       var response =  jsonDecode(result.data.toString());
       if(response["errors"]==null) {
         LanguagesModel languages = LanguagesModel.fromJson(
             jsonDecode(result.data.toString()));
         result.data = languages as LanguagesModel;
       }else{
         result.error = true;
         result.errorMessage = "Fail to get languages";
       }
      }

      return result;
    });
  }

  Future<APIRespons> deleteLanguage(String? id) async {
    String ssoUrl = "${Constants.baseUrl}graphql";

    String deleteLanguages= r'mutation deleteLanguages($input: [DeleteLanguageInput!]!) { deleteLanguages(input: $input)}';

    Map<String, String> value = new Map<String, String>();
    value["id"] = "$id";

    Map<String, dynamic> header = new Map<String, dynamic>();
    header["input"] = value;

    var jsonBody = jsonEncode({"query": deleteLanguages, "variables" : header });

    APIRequest apiRequest = APIRequest(
        url: ssoUrl,
        body: jsonBody);
    return await HttpService.instance.callService(apiRequest).then((result) {

      if(!result.error){
        var datajson = json.decode(result.data.toString());
        if(datajson["errors"] !=null){
          result.error = true;
          result.errorMessage = "No deleted";
        }

      }
      return result;
    });
  }



  Future<APIRespons> addLanguage(String langName) async {
    String ssoUrl = "${Constants.baseUrl}graphql";

    String deleteLanguages= r'mutation addLang ($input:   [InsertLanguageInput!]!){ insertLanguages(input: $input){ id languageName } }';

    Map<String, String> value = new Map<String, String>();
    value["languageName"] = "$langName";

    Map<String, dynamic> header = new Map<String, dynamic>();
    header["input"] = value;

    var jsonBody = jsonEncode({"query": deleteLanguages, "variables" : header });

    APIRequest apiRequest = APIRequest(
        url: ssoUrl,
        body: jsonBody);
    return await HttpService.instance.callService(apiRequest).then((result) {

      if(!result.error){
        var datajson = json.decode(result.data.toString());
        if(datajson["errors"] !=null){
          result.error = true;
          result.errorMessage = "No Added";
        }
        else{
          InsertLanguage insertLanguage = InsertLanguage.fromJson(datajson);
          result.data = insertLanguage.data!.insertLanguages! [0];
        }

      }
      return result;
    });
  }



  Future<APIRespons> updateLanguage(String? id, String langName) async {
    String ssoUrl = "${Constants.baseUrl}graphql";

    String deleteLanguages= r'mutation updateLang ($input:  [UpdateLanguageInput!]!){ updateLanguages(input: $input){ id languageName}}';

    Map<String, String> value = new Map<String, String>();
    value["languageName"] = "$langName";
    value["id"] = "$id";

    Map<String, dynamic> header = new Map<String, dynamic>();
    header["input"] = value;

    var jsonBody = jsonEncode({"query": deleteLanguages, "variables" : header });

    APIRequest apiRequest = APIRequest(
        url: ssoUrl,
        body: jsonBody);
    return await HttpService.instance.callService(apiRequest).then((result) {

      if(!result.error){
        var datajson = json.decode(result.data.toString());
        if(datajson["errors"] !=null){
          result.error = true;
          result.errorMessage = "Not updated";
        }
        else{
          UpdateLanguage insertLanguage = UpdateLanguage.fromJson(datajson);
          result.data = insertLanguage.data!.updateLanguages! [0];
        }

      }
      return result;
    });
  }



}
